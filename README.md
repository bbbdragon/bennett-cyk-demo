# bennett-cyk-demo

This is a simple demonstration of the CYK parsing algorithm in Clojure, written by Bennett Bullock.

It is a demonstration of my ability to take a complex algorithm and re-interpret it in a functional way.

The CYK algorithm is a determinisitc parsing algorithm for context-free grammars.  A description can be found in https://en.wikipedia.org/wiki/CYK_algorithm.

You can read more about context-free grammars in https://en.wikipedia.org/wiki/Context-free_grammar.

## Usage

Ensure you have Leiningen (version 2.8.1) with Java 1.8.0_66 installed.  It is advisable also to have
Cider running from Emacs.  A good tutorial for this can be found on https://www.braveclojure.com/.  

Assuming you have taken these steps, first go to the main directory of the project and run:
```
>lein deps
```
Now, all you need to do is run:
```
>lein run
```
and you will see a parse of the sequence "DT NN VV DT NN", or a Part-of-Speech sequence for a simple subject-verb-object sentence.

The source code for this can be found in ```src/bennett_cyk_demo/core.clj```.  Comments in this file also explain how I re-implemented the algorithm functionally.

If you would like to use this independently, you can write new rules in ```resources/grammar.txt```.  Each rule has one line, and each line has three symbols, of the form "LHS RHS1 RHS2" - "LHS" is the left-hand-side symbol, "RHS1" is the first right-hand-side symbol, and "RHS2" is the second right-hand-side symbol.  

It is your responsibility to ensure that this grammar does not generate ambiguous parses.  You can load your grammar in Clojure by doing:
```
>(read-grammar (slurp "resources/grammar.txt"))
```
Now, if you would like to run a parse from your REPL and get the parse tree (in the form of recursively embedded vectors, first, in your Cider REPL, change your namespace to the main file:
```
(ns bennett-cyk-demo.core)
```
Make sure you've compiled ```core.clj``` by opening this file in a buffer and doing C-c C-k.

Now, you can run the code as:
```
>(def grammar (read-grammar (slurp "resources/grammar.txt")))
>(def seq-string "DT NN VV DT NN")
>(parse seq-string grammar)
["S" ["NP" ["DT"] ["NN"]] ["VP" ["VV"] ["NP" ["DT"] ["NN"]]]]
```
You can also run it in a slightly more readable format as:
```
>(print-tree seq-string grammar)
S
  NP
    DT
    NN
  VP
    VV
    NP
      DT
      NN
```